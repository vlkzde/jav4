Java: Concurrent Programming - vícevláknové programování v Javě
===============================================================

- **Lektor:** Ing. Jan kolomazník, Ph.D.
- **Mobil:** +420 732 568 669
- **Email:** jan.kolomaznik@gmail.com 

Školení je určeno především pokročilejším vývojářům. 
Ačkoliv část úvodu je věnována zopakování terminologie a základních konceptů (thread, race condition apod.), neměly by tyto koncepty být pro posluchače úplnou novinkou. 
Kurz popisuje vývoj simultánních procesů s Javou. 
To zahrnuje koncepty paralelního programování, neměnnosti, vláken atd.

Osnova kurzu
------------

- **Úvod do concurrency**
    - Thread, multithreading, paralelní zpracování
    - Výhody a úskalí; race conditions, deadlocks, livelocks, dopady na výkonnost
- **Exekuční model**
    - Pořadí vykonávání, uspořádání operací, paměťová viditelnost
    - Java Memory Model, relace happens-before
    - Využití
- **Concurrency v teorii**
    - Spravování stavu, nemodifikovatelnost
    - Izolace a její techniky
    - Bezpečná publikace objektů
- **Concurrency v praxi**
    - Idiomy: double-checked locking, lazy initialization holder, final wrapper
    - Instance pooling
    - Návrh thread-safe tříd; návrhové postupy a problémy
- **Základní podpora multithreadingu**
    - Monitory: JVM and Java, používání vestavěných zámků a podmínkových proměnných
    - Thread: vykonávání, blokování a přerušení
    - Ukončování JVM
- **Threading framework**
    - Thread pools, Executor framework, Fork Join Pool
    - Návrh založený na aktivitách, plánování, futures
    - Problém ukončení aktivit
- **Stavební bloky**
    - Synchronizace s nízkou režií: atomické proměnné, volatile
    - Synchronizační primitiva: zámky, semafory, bariéry, čítače atd.
    - Synchronized & concurrent collections
- **Problémy**
    - Deadlocks, livelocks, starvation: příčiny a řešení, techniky
    - Amdahlův zákon, cena za paralelismus, výkonnost systému
    - Problém „úzkého hrdla“, optimalizační strategie
    - Neblokující techniky
- **Některé další frameworky**
    - NIO: neblokující I/O
    - NIO.2: asynchronní I/O
    - Fork Join Pool, podpora asynchronních úloh
