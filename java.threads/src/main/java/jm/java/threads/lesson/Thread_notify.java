package jm.java.threads.lesson;

public class Thread_notify extends Thread {

    private Object monitor = new Object();

    private int value = -1;

    public Thread_notify(String jmeno) {
        super(jmeno);
    }

    public void run() {
        for (int i = 1; i <= 3; i++) {
            System.out.println(i + ". " + getName());
            value = i;
            synchronized (monitor) {
                monitor.notify();
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println("Jsem vzhuru - " + getName());
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread_notify vl = new Thread_notify("Pepa");
        new Thread(() -> {
            while (!interrupted()) {
                try {
                    synchronized (vl.monitor) {
                        vl.monitor.wait();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return;
                }
                System.out.println(Thread.currentThread().getName() + ":" + vl.value);
            }
        }).start();
        new Thread(() -> {
            while (!interrupted()) {
                try {
                    synchronized (vl.monitor) {
                        vl.monitor.wait();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return;
                }
                System.out.println(Thread.currentThread().getName() + ":" + vl.value);
            }
        }).start();
        new Thread(() -> {
            while (!interrupted()) {
                try {
                    synchronized (vl.monitor) {
                        vl.monitor.wait();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return;
                }
                System.out.println(Thread.currentThread().getName() + ":" + vl.value);
            }
        }).start();
        vl.start();
        Thread.sleep(2500);
        vl.interrupt();
    }
}