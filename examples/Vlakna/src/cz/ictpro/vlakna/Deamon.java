package cz.ictpro.vlakna;

import java.util.Date;

public class Deamon {

    public static void main(String[] args) throws InterruptedException {
        Thread monitor = new Thread(Deamon::monitor);
        monitor.setDaemon(true);
        monitor.start();

        Thread.sleep(10000);
        System.out.println("Main done");
    }

    public static void monitor() {
        try {
            while (!Thread.interrupted()) {
                System.out.println("Monitor " + new Date());
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            System.out.println("Monitor finally.");
        }
    }
}
