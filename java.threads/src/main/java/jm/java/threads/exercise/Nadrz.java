package jm.java.threads.exercise;

public class Nadrz {

    private long obsah = 1_000_000;

    public synchronized long getObsah() {
            return obsah;
    }

    public synchronized void inc() {
            this.obsah++;
    }

    public synchronized void dec() {
            this.obsah--;
    }
}
