package cz.ictpro.vlakna;

public class Point {

    private int x, y;

    public static int count() {
        synchronized (Point.class) {
            return 1;
        }
    }

    public int getX() {
        synchronized (this) {
            return x;
        }
    }

    public void setX(int x) {
        synchronized (this) {
            this.x = x;
        }
    }

    @Override
    public synchronized String toString() {
        return super.toString();
    }


}
