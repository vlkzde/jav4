package jm.java.threads.exercise;

public class Pumpa extends Thread {


    private Nadrz a;
    private Nadrz b;
    private Object trubka;

    public Pumpa(Nadrz a, Nadrz b, Object trubka) {
        this.a = a;
        this.b = b;
        this.trubka = trubka;
    }

    @Override
    public void run() {
        while (!interrupted()) {
            //synchronized (trubka) {
                a.dec();
                b.inc();
            //}
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Object trubka = new Object();
        Nadrz n1 = new Nadrz();
        Nadrz n2 = new Nadrz();
        Pumpa p1 = new Pumpa(n1, n2, trubka);
        Pumpa p2 = new Pumpa(n2, n1, trubka);
        p1.start();
        p2.start();

        long count = 0;
        while (true) {
            Thread.sleep(1000);
            if (count++ == 10) {
                p1.interrupt();
                p2.interrupt();
                p1.join();
                p1.join();
            }
            synchronized (trubka) {
                long a = n1.getObsah();
                long b = n2.getObsah();
                System.out.printf("Suma: %d, n1: %d, n2: %d\n", (a+b), a, b);
            }
        }
    }
}
