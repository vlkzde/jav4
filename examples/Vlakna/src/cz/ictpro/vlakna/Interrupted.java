package cz.ictpro.vlakna;

public class Interrupted {

    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread(Interrupted::forEverLoopWithSleep, "Task 1");
        t.start();

        Thread.sleep(3000);

        t.interrupt();
        System.out.println("t.interrupt()");

        Thread.sleep(3000);
        if (t.isAlive()){
            t.stop();
            System.out.println("t.stop()");
        }
    }

    public static void forEverLoop() {
        String name = Thread.currentThread().getName();
        long time = 0;
        try {
            while (true) {
                if (++time % 100_000_000 == 0) {
                    System.out.println("For ever loop: " + name);
                }
            }
        } finally {
            System.out.println("Done: " + time);
        }
    }

    public static void forEverLoopNice() {
        Thread currentThread = Thread.currentThread();
        String name = currentThread.getName();
        long time = 0;
        try {
            while (true) {
                if (++time % 100_000_000 == 0) {
                    System.out.println("For ever loop: " + name);
                    if (currentThread.isInterrupted()) {
                        return;
                    }
                }
            }
        } finally {
            System.out.println("Done: " + time);
        }
    }

    public static void forEverLoopWithSleep() {
        Thread currentThread = Thread.currentThread();
        String name = currentThread.getName();
        try {
            while (!currentThread.isInterrupted()) {
                System.out.println("For ever loop: " + name);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            System.out.println("Interrupted: " + currentThread.isInterrupted());
            System.out.println("Interrupted: " + name);
        } finally {
            System.out.println("Done: " + name);
        }
    }
}
