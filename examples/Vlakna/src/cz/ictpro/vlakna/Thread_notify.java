package cz.ictpro.vlakna;

public class Thread_notify extends Thread {

    private Object monitor = new Object();

    private int value = -1;

    public Thread_notify(String jmeno) {
        super(jmeno);
    }

    public void run() {
        for (int i = 1; i <= 5; i++) {
            System.out.println(i + ". " + getName());
            synchronized (monitor) {
                value = i;
                monitor.notifyAll();
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println("Jsem vzhuru - " + getName());
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread_notify vl = new Thread_notify("Pepa");
        new Thread(() -> {
            while (!interrupted()) {
                try {
                    synchronized (vl.monitor) {
                        vl.monitor.wait();
                        System.out.println(Thread.currentThread().getName() + ":" + vl.value);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return;
                }
            }
        }).start();
        new Thread(() -> {
            while (!interrupted()) {
                try {
                    synchronized (vl.monitor) {
                        vl.monitor.wait();
                        System.out.println(Thread.currentThread().getName() + ":" + vl.value);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return;
                }
            }
        }).start();
        new Thread(() -> {
            while (!interrupted()) {
                try {
                    synchronized (vl.monitor) {
                        vl.monitor.wait();
                        System.out.println(Thread.currentThread().getName() + ":" + vl.value);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return;
                }
            }
        }).start();
        vl.start();
        //Thread.sleep(2500);
        //vl.interrupt();
    }

}
