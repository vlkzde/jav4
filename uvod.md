[remark]:<class>(center, middle)

Paralelní programování
======================
Uvod
----

[remark]:<slide>(new)
## Paralelismus

* Základní úrovně:
  * hardwarová (procesory, jádra)
  * programová (procesy, vlákna)
  * algoritmická (uf... )
  
* Motivace:
  * zvýšení výkonu
  * redundance
    * jiné cíle, ale podobné nástroje a problémy

[remark]:<slide>(new)
## Proč zvyšovat výkon?

* Moorův zákon:
  * „... Počet tranzistorů/komponent, které lze levně integrovat se zdvojnásobí každý rok...“ 
    * 12 měsíců (1965)
    * 18 měsíců (1985)
    * 24 měsíců (2005)
[remark]:<slide>(wait)
    
  * Jedná se spíš o strategii Intelu než o zákon
  * Dodržuje se zvyšováním počtu jader
    * nejsou integrované na jednom polovodiči

[remark]:<slide>(new)
## Skutečný průběh

![moores law](media/moores-law.png)

[remark]:<slide>(new)
## Počet tranzistorů vs. výkon

![moores law](media/transistors-power.png)

[remark]:<slide>(new)
## Počet tranzistorů vs. výkon

![moores law](media/transistors-power-2.png)

[remark]:<slide>(new)
## Frekvence

* Posledních 10 let se drží okolo 3Ghz, růst se neočekává.
* Rychlost elektronů je konečná, tranzistory mají konečnou velikost.

![moores law](media/frekvence.png)

[remark]:<slide>(new)
## Terminologie

* Paralelní systém – systém, ve kterém může probíhat několik **procesů/činností** současně

* Rozlišovací úroveň
  * instrukce, iterace, procedura, vlákno, proces, úloha

* Proces × Vlákno
  * *vlákno* – instance kódu + dat a stav zásobníku + registrů
  * *proces* – kolekce vláken, společný kód + data

* Procesor × Jádro
  * jádro (core) – čip + paměť + řadič vykonávající jedno vlákno
  * procesor – kolekce jader
  * multithreaded-core
  * multiprocesor = multicore

[remark]:<slide>(new)
## Vlákna vs. procesy

* Slouží k paralelizování části programu.

* Kdy využít nové vlákno nebo nový proces?

* Hlavním rozdílem, mezi procesem a vláknem je sdílení paměti. Zatímco proces je robustní a samostatný celek, který má
  všechnu paměť sám pro sebe, vlákno sdílí svoji paměť s dalšími vlákny.

⇒ *V aplikacích se používají vlákna*

[remark]:<slide>(new)
### Výkon programu

- P) Vytvoření nového procesu vyžaduje kopii původního procesu a jeho dat. ⇒ **Značná režie**.

- V) Při vytvoření vlákna se nemusí kopírovat proces ani data. ⇒ **Vlákno je levné**.

[remark]:<slide>(wait)
- P) Přepnutí kontextu u procesu je **poměrně dlouhá a náročná činnost**.

- V) Přepnutí kontextu u vláken je **méně náročné**, než v případě procesu.

[remark]:<slide>(new)
### Práce s pamětí
- P) Procesy si **nemohou** navzájem přepsat svoji paměť.

- V) Vlákno **může** přepsat paměť se kterou pracuje jiné vlákno a vytvořit tím chybu v programu.

[remark]:<slide>(wait)
- P) Každý proces musí mít svou vlastní paměť s vlastními kopiemi proměnných. ⇒ Procesy zabírají **více paměti**.

- V) Vlákna **sdílí společný paměťový prostor** a díky tomu může program spotřebovávat méně systémových prostředků.

???
Jsou výjimky umožňující tvořit  a použití sdílené paměti (Shared memory), což je metoda poskytovaná systémem v rámci mezi-procesové komunikace (IPC – Inter-process communication).

[remark]:<slide>(new)
### Odolnost proti chybám
- P) Chyba v jednom procesu **neovlivní** ostatní procesy.

- V) Chyba v jednom vlákně může **shodit celý proces** se všemi běžícími vlákny.

[remark]:<slide>(new)
### Vzájemná komunikace
- P) Procesy mezi sebou mohou komunikovat pouze pomocí **prostředků IPC**.

     - *Jaké prostředky to jsou, záleží na operačním systému*.

     - *V Linuxu do IPC patří například: Soubory, signály, sokety, zprávy, roury, pojmenované roury, sdílená paměť, soubor namapovaný do paměti, RPC (Remote Procedure Calls)*.

- V) Vlákna **nepotřebují** žádný speciální mechanismus na komunikaci, protože mají společný paměťový prostor.

[remark]:<slide>(new)
### Synchronizace
- P+V) **Synchronizace v přístupu** k sdíleným datům je nutná jak v případě vláken tak procesů.

- P) Proces může ke **svým vlastním datům** přistupovat bez omezení.

- V) Vlákna **vlastní data nemají**, všechny data jsou sdílená, a proto je třeba všechny přístupy do paměti synchronizovat.

[remark]:<slide>(new)
### Priority
- P) Změna priority rodiče nemá vliv na proces, který je potomkem.

- V) Změna priority hlavního vlákna procesu se může odrazit na všech vláknech.

[remark]:<slide>(new)
## Vytváření procesů
* Procesy reprezentuje třída `java.lang.Process`.
* Vytvořit ji lze dvěma způsoby:

```java
Runtime.getRuntime().exec(command);
```

```java
ProcessBuilder pb = new ProcessBuilder("myCommand", "myArg1", "myArg2");
Map<String, String> env = pb.environment();
env.put("VAR1", "myValue");
env.remove("OTHERVAR");
pb.directory(new File("myDir"));
File log = new File("log");
pb.redirectErrorStream(true);
pb.redirectOutput(Redirect.appendTo(log));
Process p = pb.start();
p.waitFor();
```

[remark]:<slide>(new)
## Komunikace s procesy
* Probíhá pomocí prostředků poskytovaných operačním systém
* Nejsnadnější způsob je pomocí `stdin`/`stdout`

**Zaslání zprávy procesu `p`**

```java
OutputStream stdin = p.getOutputStream();
stdin.write(message);
stdin.flush();
```

**Čtení zprávy od procesu `p`**

```java
InputStream stdout = p.getInputStream();
message = stdin.read();
```

[remark]:<slide>(new)
## Vytváření vláken
* Vlákna reprezentuje objekt `java.lang.Thread`
* Vytvářejí se příkazem

```java
Thread thread = new Thread();
thread.start();
```

* Takto vytvořená vlákna neprovádějí žádný kód: musí se:
    * Vyvořit vlastní potomek
    * Implementovat rozhraní `Runnable`

[remark]:<slide>(new)
### Vytvoření vlastního potomka
* Nutné přetížit metodu `public void run()`

```java
public class MyThread extends Thread {

  public void run(){
    System.out.println("MyThread running");
  }
}
```

[remark]:<slide>(new)
### Vytvoření vlastního potomka *anonymní třída*
* Možno použít rovněž anonymní implementaci:

```java
Thread thread = new Thread(){
    public void run(){
      System.out.println("Thread Running");
    }
}

thread.start();
```

[remark]:<slide>(new)
### Implementace rozhraní *Runneable*
* Hlavní výhodou je, že může být implementováno libovolnou třídu
* V Java 8 velmi úsporné díky použití lambda výrazů

```java
Runnable myRunnable = new Runnable(){
  public void run(){
    System.out.println("Runnable running");
  }
}

Thread thread = new Thread(myRunnable);
thread.start();
```

[remark]:<slide>(new)
#### Pozor! častá chyba!
* volání metody `run()` místo `start()`

```java
Thread newThread = new Thread(MyRunnable());
thread.run();  //should be start();
```

[remark]:<slide>(new)
## Třída *Thread*
* Lze nastavi jméno (lepší přehlednost)

```java
MyRunnable runnable = new MyRunnable();
Thread t = new Thread(runnable, "New Thread");
```

[remark]:<slide>(wait)
* Získání instance aktuálního vlákna.

```java
Thread.currentThread();
```

[remark]:<slide>(new)
## Třída *Thread*
* Příklad získání jména aktuálního vlákna.

```java
String threadName = Thread.currentThread()
                          .getName();
```

[remark]:<slide>(wait)
* Změna priority.

```java
thread.setPriority(MAX_PRIORITY);
```

* Nastavení vlákna jako `Deamon`, aplikace běží pokud běží alespoň jedné vlákno typu deamon.

[remark]:<slide>(new)
## Vláknově (ne)bezpečné proměnné
* Lokální proměnná jsou vždy.

```java
public void someMethod(){
  long threadSafeInt = 0;
  threadSafeInt++;
}
```

[remark]:<slide>(new)
* Reference na lokální proměnné pouze pokud neopustí metodu.

```java
public void someMethod(){
  LocalObject localObject = new LocalObject();
  localObject.callMethod();
  method2(localObject);
}

public void method2(LocalObject localObject){
  localObject.setValue("value");
}
```

[remark]:<slide>(new)
## Vláknově nebezpečné proměnné
* Proměnné instance nebo proměnné třídy *nejsou vláknově bezpečné*.

```java
NotThreadSafe sharedInstance = new NotThreadSafe();
new Thread(new MyRunnable(sharedInstance)).start();
new Thread(new MyRunnable(sharedInstance)).start();
public class MyRunnable implements Runnable {
  NotThreadSafe instance = null;
  public MyRunnable(NotThreadSafe instance){
    this.instance = instance;
  }
  public void run(){
    this.instance.add("some text");
  }
}
```

[remark]:<slide>(new)
### Immutable objekty jsou Vláknově bezpečné
* Proto se je snažte používat co nejčastěji :)

```java
public class ImmutableValue{
  private int value = 0;
  public ImmutableValue(int value){
    this.value = value;
  }
  public int getValue(){
    return this.value;
  }
  public ImmutableValue add(int valueToAdd){
      return new ImmutableValue(this.value + valueToAdd);
  }
}
```

[remark]:<slide>(new)
* Ale POZOR! Přestože immutable jsou vláknově bezpečné, s jejich referencemi to již neplatí.

```java
public class Calculator{
  private ImmutableValue currentValue = null;
  public ImmutableValue getValue(){
    return currentValue;
  }
  public void setValue(ImmutableValue newValue){
    this.currentValue = newValue;
  }
  public void add(int newValue){
    this.currentValue = this.currentValue.add(newValue);
  }
}
```

[remark]:<slide>(new)
## Koncepty
* Komunikace – výměna dat mezi paralelně probíhajícími procesy

* Synchronizace – zaručení definované posloupnosti operací, logika procesů

* Režie (overhead) paralelismu 
  * čas spotřebovaný na komunikaci
  * čas spotřebovaný na synchronizaci a výměnu dat
  * ostatní režie (knihovny, spuštění, zastavení)

* Škálovatelnost (scalability)
  * míra růstu výkonosti s růstem paralelismu

[remark]:<slide>(new)
## Paralelizace – HW

* Flynnova klasifikace systému
  * staré (1966), hrubé, ale používané
  * Single Instruction, Single Data stream (SISD)
  * Single Instruction, Multiple Data streams (SIMD)
  * Multiple Instruction, Single Data stream (MISD)
    * Multiple Single Instruction stream, Multiple Data stream (MSIMD) – několik propojených SIMD
    * Same Program, Multiple Data stream (SPMD) – každý procesor provádí stejný program, využívají se běžné CPU
  * Multiple Instruction, Multiple Data streams (MIMD)

[remark]:<slide>(new)

## Práce Javy s pamětí - logický pohled
* Pro porozumění práce více vláknových aplikací je velmi důležité rozumět tomu, jak JVM pracuje s pamětí.
* Java rozděluje paměť na zásobníky vláken a heap

![Rozdělení paměti](media/java-memory-model-1.png)

[remark]:<slide>(new)
class: middle
* Na zásobníku jsou uloženy všechny lokální proměnné a reference na objekty.

[remark]:<slide>(new)
### Uspořádání v dat v paměti - logický pohled
* Platí že:
    * Primitivní datové typu jsou uložená na zásobníku.
    * Objekty jsou uloženy na heap.

![Uspořádání v paměti](media/java-memory-model-3.png)

[remark]:<slide>(new)
## Hardware mapování logického pohledu
* Jak se dá očekávat, je poněkud odlišné o logické architektury, velmi zjednodušená vypadá nějak takto ...

![Hardware model paměti](media/java-memory-model-4.png)

[remark]:<slide>(new)
## Mapování mezi modely
* Logický a fyzický model je nutné namapovat na sebe navzájem.

![Mapování modelů](media/java-memory-model-5.png)

[remark]:<slide>(new)
class: middle
* Dva hlavní problémy:
    * Viditelnost mezi vlákny změněných proměnných
    * Kontrola přístupu při čtení a zapisování sdílených proměnných

[remark]:<slide>(new)
### Viditelnost sdílených proměnných
* problém s proměnnými uloženými v lokálních Caches bez řádné synchronizace

![Sdílené promněnné](media/java-memory-model-6.png)

[remark]:<slide>(new)
class: middle
* Klíčové slovo `volatile` zabrání uložení proměnné do CPU Cache.

```java
public volatile int counter = 0;
```

[remark]:<slide>(new)
### Proměnné sdílené přes více vláken
* klíčové slovo `volatile` použijeme pokud potřebujeme zajistit viditelnost změn proměnné mezi různými vlákny.

![Volatile](media/java-volatile-1.png)

[remark]:<slide>(new)
### Synchronizace proměnných
* Druhým problémem je synchronizace práce s proměnnou při souběžném update.

![Sdílené promněnné](media/java-memory-model-7.png)

[remark]:<slide>(new)
class: middle
* Tento problém se řeší prostřednictvím klíčového slova `synchronized`.

```java
  public synchronized void add(int value){
      this.count += value;
  }
```

[remark]:<slide>(new)
## Amdahlův zákon
* 𝑓_𝑠−𝑓_𝑝=1
* 𝑓_𝑠 – podíl programu, který nelze paralelizovat
* 𝑓_𝑝 – podíl programu, který lze paralelizovat
* t – celková doba výpočtu
* p – počet procesorů

* 𝑆_𝑧=𝑡/(𝑡⋅𝑓_𝑠+𝑡⋅𝑓_𝑝/𝑝)=1/(1−𝑓_𝑝+𝑓_𝑝/𝑝)

[remark]:<slide>(new)
## Amdahlův zákon

![moores law](media/amdahl-law-1.png)

[remark]:<slide>(new)
## Amdahlův zákon

![moores law](media/amdahl-law-2.png)

[remark]:<slide>(new)
## Možnosti paralelizace

* Nízko úrovňové (fine grained)
  * pipelining, multithreading, a podobné vychytávky
  * pomáhá při běžných operacích
  * neřešíme

* Vyšší úroveň
  * použití více jader / procesorů
    * pod 60% 𝑓_𝑝 zcela nezajímavé
  * paralelizace algoritmu
    * zvyšujeme 𝑓_𝑝
  *využití speciálního HW (GPU)

[remark]:<slide>(new)
## Co musíme vyřešit

* synchronizace
* přístup ke sdílené paměti
  * různé modely
* distribuce dat
* distribuce instrukcí (práce)
* komunikace
* minimalizovat režii
* použít vhodný HW
* měření
